
    create table agent (
       id bigint not null auto_increment,
        address varchar(255),
        description varchar(255),
        email varchar(255),
        image_path varchar(255),
        name varchar(255),
        phone_no varchar(255),
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table career (
       id bigint not null auto_increment,
        description varchar(255),
        list_of_jobs varchar(255),
        name varchar(255),
        value varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table news (
       id bigint not null auto_increment,
        author varchar(255),
        content varchar(255),
        date datetime,
        title varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table post (
       id bigint not null auto_increment,
        content varchar(255),
        date datetime,
        flag varchar(255),
        likes integer not null,
        type varchar(255),
        username varchar(255),
        student_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table quizquestion (
       id bigint not null auto_increment,
        content varchar(255),
        question_no varchar(255),
        value varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table role (
       id bigint not null auto_increment,
        role varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table student (
       id bigint not null auto_increment,
        about varchar(255),
        email varchar(255),
        grade varchar(255),
        image_path varchar(255),
        interest varchar(255),
        name varchar(255),
        school varchar(255),
        career_result_id bigint,
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table university (
       id bigint not null auto_increment,
        description varchar(255),
        image_path varchar(255),
        name varchar(255),
        phone_no varchar(255),
        website varchar(255),
        user_id bigint,
        primary key (id)
    ) engine=InnoDB

    create table user (
       id bigint not null auto_increment,
        enabled integer,
        password varchar(255),
        username varchar(255),
        primary key (id)
    ) engine=InnoDB

    create table user_role (
       user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    ) engine=InnoDB

    alter table agent 
       add constraint FKneqbqf7p5lfvciwroct25kasc 
       foreign key (user_id) 
       references user (id)

    alter table post 
       add constraint FKca3ceikkva3wec2ioshfqy9ha 
       foreign key (student_id) 
       references student (id)

    alter table student 
       add constraint FK2pefyjc3f23xuthyl778h68qt 
       foreign key (career_result_id) 
       references career (id)

    alter table student 
       add constraint FKk5m148xqefonqw7bgnpm0snwj 
       foreign key (user_id) 
       references user (id)

    alter table university 
       add constraint FKpu7u12w0lv8j4fati6lne988k 
       foreign key (user_id) 
       references user (id)

    alter table user_role 
       add constraint FKa68196081fvovjhkek5m97n3y 
       foreign key (role_id) 
       references role (id)

    alter table user_role 
       add constraint FK859n2jvi8ivhui0rl0esws6o 
       foreign key (user_id) 
       references user (id)
