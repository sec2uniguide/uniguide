package com.sec2uniguide.uniguide.domains;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
@Entity
@Table(name="news")
public class News {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotBlank(message="content is required")
	private String content;
	
	@NotBlank(message="title is required")
	private String title;
	
	private String author;
	
	private Date date;
	
	@PrePersist
    void setDate() {
    	this.date=new Date();
    }
}	
	

