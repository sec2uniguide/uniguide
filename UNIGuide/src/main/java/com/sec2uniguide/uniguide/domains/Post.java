package com.sec2uniguide.uniguide.domains;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
@Entity
@Table(name="post")
public class Post {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	
	@NotBlank(message="content is required")
	private String content;
	
	private Date date;
	
    @PrePersist
    void setDate() {
    	this.date=new Date();
    }
	
	
	private int likes;
	
	public String flag;

	private String type;
	
	private String username;
	
	@ManyToOne
	private Student student;
	
	
}
