package com.sec2uniguide.uniguide.domains;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.sec2uniguide.uniguide.security.User;

import lombok.Data;

@Data
@Entity
@Table(name="student")
public class Student {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private  long id;
	
	private String name;
	
	@Column(name = "image_path")
	private String imagePath;
	
	private String email;
	
	private String school;
	
	private String grade;
		
	private String interest;
	
	private String about;
	
	@OneToOne
	private User user;
	
	@OneToOne
	private Career careerResult;
}
