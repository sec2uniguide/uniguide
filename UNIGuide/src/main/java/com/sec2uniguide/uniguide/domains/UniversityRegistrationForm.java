package com.sec2uniguide.uniguide.domains;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import com.sec2uniguide.uniguide.security.User;

import lombok.Data;

@Data
public class UniversityRegistrationForm {

	@NotBlank(message = "Please provide a username")
	private String username;
	
	@Size(min = 5, message = "Your password must have at least 5 characters")
	@NotBlank(message = "Please provide your password")
	private String password;

	@NotBlank(message="Name is required")
	private String name;
	
	private MultipartFile imageInput;
	
	@NotBlank(message="website is required")
	private String website;
	
	@NotBlank
	(message="description is required")
	private String description;	
	
	@NotBlank(message="phone number is required")
	@Size(min=12,max=14,message="please enter a correct phone number")
	private String phoneNo;

	public User getUser() {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		return user;
	}
	
private String storeImage(MultipartFile file) {
		
		Path rootLocation = Paths.get("uploads");
		
		String filename = "profile_" + this.username + "_" + file.getOriginalFilename();
        try {
            if (file.isEmpty()) {
                System.out.println("File is empty");
            }
            if (filename.contains("..")) {
                // This is a security check
               System.out.println("Could not store file");
            }
            byte[] bytes = file.getBytes();
            Path path = Paths.get("src/main/resources/static/uploads/" + filename);
            Files.write(path, bytes);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        
        return rootLocation.resolve(filename).toString();
	}
	
	public University getUniversity() {
		University university = new University();
		university.setName(name);
		university.setImagePath(storeImage(imageInput));
		university.setWebsite(website);
		university.setPhoneNo(phoneNo);
		university.setDescription(description);
		return university;
	}
}
