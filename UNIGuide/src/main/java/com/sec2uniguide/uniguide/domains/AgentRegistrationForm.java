package com.sec2uniguide.uniguide.domains;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import com.sec2uniguide.uniguide.security.User;

import lombok.Data;

@Data
public class AgentRegistrationForm {

	@NotBlank(message = "Please provide a username")
	private String username;
	
	@Size(min = 5, message = "Your password must have at least 5 characters")
	@NotBlank(message = "Please provide your password")
	private String password;
	
	@NotBlank(message="Name is required")
	private String name;
	
	private MultipartFile imageInput;
	
	@NotBlank(message="Email is required")
	@Email
	private String email;
	
	@NotBlank(message="description is required")
	private String description;	
	
	@NotBlank(message="phone number is required")
	@Size(min=12,max=14,message="please enter a correct phone number")
	private String phoneNo;
	
	@NotBlank(message="Address is required")
	private String address;
	
	
	public User getUser() {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		return user;
	}
	
private String storeImage(MultipartFile file) {
		
		Path rootLocation = Paths.get("uploads");
		
		String filename = "profile_" + this.username + "_" + file.getOriginalFilename();
        try {
            if (file.isEmpty()) {
                System.out.println("File is empty");
            }
            if (filename.contains("..")) {
                // This is a security check
               System.out.println("Could not store file");
            }
            byte[] bytes = file.getBytes();
            Path path = Paths.get("src/main/resources/static/uploads/" + filename);
            Files.write(path, bytes);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        
        return rootLocation.resolve(filename).toString();
	}
	
	public Agent getAgent() {
		Agent agent = new Agent();
		agent.setName(name);
		agent.setImagePath(storeImage(imageInput));
		agent.setEmail(email);
		agent.setPhoneNo(phoneNo);
		agent.setAddress(address);
		agent.setDescription(description);
		return agent;
	}
	
}
