package com.sec2uniguide.uniguide.domains;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.web.multipart.MultipartFile;

import com.sec2uniguide.uniguide.security.User;

import lombok.Data;

@Data
public class StudentRegistrationForm {

	@NotBlank(message = "Please provide a username")
	private String username;
	
	@NotBlank(message = "Please provide your password")
	@Size(min = 5, message = "Your password must have at least 5 characters")	
	private String password;
	
	@NotBlank(message="name is required")
	private String name;
	
	private MultipartFile imageInput;
	
	@NotBlank(message="email is required")
	@Email
	private String email;
	
	@NotBlank(message="school is required")
	private String school;
	
	@NotBlank(message="grade is required")
	private String grade;
	
	@NotBlank(message="Interest is required")
	private String interest;
	
	@NotBlank(message="Description is required")
	private String description;
	
	public User getUser() {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		return user;
	}
	
private String storeImage(MultipartFile file) {
		
		Path rootLocation = Paths.get("uploads");
		
		String filename = "profile_" + this.username + "_" + file.getOriginalFilename();
        try {
            if (file.isEmpty()) {
                System.out.println("File is empty");
            }
            if (filename.contains("..")) {
                // This is a security check
               System.out.println("Could not store file");
            }
            byte[] bytes = file.getBytes();
            Path path = Paths.get("src/main/resources/static/uploads/" + filename);
            Files.write(path, bytes);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        
        return rootLocation.resolve(filename).toString();
	}
	
	
	public Student getStudent() {
		Student student = new Student();
		student.setName(name);
		student.setImagePath(storeImage(imageInput));
		student.setEmail(email);
		student.setSchool(school);
		student.setGrade(grade);
		student.setInterest(interest);
		student.setAbout(description);
		return student;
	}
}
