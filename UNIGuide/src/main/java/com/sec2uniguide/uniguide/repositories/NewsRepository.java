package com.sec2uniguide.uniguide.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sec2uniguide.uniguide.domains.News;
public interface NewsRepository extends CrudRepository<News ,Long> {


}