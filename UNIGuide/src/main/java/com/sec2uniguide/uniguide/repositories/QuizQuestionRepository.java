package com.sec2uniguide.uniguide.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sec2uniguide.uniguide.domains.QuizQuestion;
public interface QuizQuestionRepository extends CrudRepository<QuizQuestion ,Long> {


}
