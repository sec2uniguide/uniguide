package com.sec2uniguide.uniguide.repositories;
import org.springframework.data.repository.CrudRepository;

import com.sec2uniguide.uniguide.domains.Post;
import com.sec2uniguide.uniguide.domains.Student;
public interface StudentRepository extends CrudRepository<Student ,Long> {

Student findByUserId(Long user_id);
}
