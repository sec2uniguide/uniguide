package com.sec2uniguide.uniguide.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sec2uniguide.uniguide.security.User;

public interface UserRepository extends CrudRepository<User, Long>{

	User findByUsername(String username);
}
