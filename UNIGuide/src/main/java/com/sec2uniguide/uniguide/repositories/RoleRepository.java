package com.sec2uniguide.uniguide.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sec2uniguide.uniguide.security.Role;

public interface RoleRepository extends CrudRepository<Role, Long>{

	Role findByRole(String role);
}
