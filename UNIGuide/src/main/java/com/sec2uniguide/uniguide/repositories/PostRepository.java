package com.sec2uniguide.uniguide.repositories;

import org.springframework.data.repository.CrudRepository;

import com.sec2uniguide.uniguide.domains.Post;
public interface PostRepository extends CrudRepository<Post ,Long> {

Iterable<Post> findByType(String type);
}
