package com.sec2uniguide.uniguide.repositories;
import org.springframework.data.repository.CrudRepository;

import com.sec2uniguide.uniguide.domains.University;
public interface UniversityRepository extends CrudRepository<University ,Long> {

	University findByUserId(Long user_id);
}