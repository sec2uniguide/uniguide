package com.sec2uniguide.uniguide.repositories;
import org.springframework.data.repository.CrudRepository;

import com.sec2uniguide.uniguide.domains.Agent;
public interface AgentRepository extends CrudRepository<Agent ,Long> {

	Agent findByUserId(Long user_id);
}

