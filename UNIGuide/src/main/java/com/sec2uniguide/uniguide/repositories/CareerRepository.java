package com.sec2uniguide.uniguide.repositories;
import org.springframework.data.repository.CrudRepository;

import com.sec2uniguide.uniguide.domains.Career;
public interface CareerRepository extends CrudRepository<Career ,Long> {


}
