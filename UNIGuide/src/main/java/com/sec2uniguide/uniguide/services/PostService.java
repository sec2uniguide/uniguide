package com.sec2uniguide.uniguide.services;

	import java.util.Optional;

import com.sec2uniguide.uniguide.domains.Post;

	public interface PostService {
		public Post save(Post post);
		
		public Iterable<Post> saveAll(Iterable<Post> posts);

		Optional<Post> findById(Long id);

		boolean existsById(Long id);
		
		Iterable<Post> findAll();

		Iterable<Post> findAllById(Iterable<Long> ids);
		Iterable<Post> findByType(String type);

		long count();
		
		void deleteById(Long id);
		
		void delete(Post post);
		
		void deleteAll(Iterable<Post> posts);

		void deleteAll();
	}


