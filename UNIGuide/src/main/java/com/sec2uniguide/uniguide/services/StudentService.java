package com.sec2uniguide.uniguide.services;

import java.util.Optional;

import com.sec2uniguide.uniguide.domains.Student;

public interface StudentService {
	public Student save(Student student);
	
	public Iterable<Student> saveAll(Iterable<Student> students);

	Optional<Student> findById(Long id);
	Student findByUserId(Long user_id);
	boolean existsById(Long id);
	
	Iterable<Student> findAll();

	Iterable<Student> findAllById(Iterable<Long> ids);

	long count();
	
	void deleteById(Long id);
	
	void delete(Student student);
	
	void deleteAll(Iterable<Student> students);

	void deleteAll();
}

