package com.sec2uniguide.uniguide.services;
import java.util.Optional;

import com.sec2uniguide.uniguide.domains.Agent;

public interface AgentService {
	public Agent save(Agent agent);
	
	public Iterable<Agent> saveAll(Iterable<Agent> agents);

	Optional<Agent> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<Agent> findAll();

	Iterable<Agent> findAllById(Iterable<Long> ids);
	
	Agent findByUserId(Long user_id);

	long count();
	
	void deleteById(Long id);
	
	void delete(Agent agent);
	
	void deleteAll(Iterable<Agent> agents);

	void deleteAll();
}
