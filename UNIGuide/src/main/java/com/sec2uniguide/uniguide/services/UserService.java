package com.sec2uniguide.uniguide.services;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.sec2uniguide.uniguide.security.User;

public interface UserService  extends UserDetailsService {

	User findUserByUsername(String username);
	void saveUser(User user, String role);
}
