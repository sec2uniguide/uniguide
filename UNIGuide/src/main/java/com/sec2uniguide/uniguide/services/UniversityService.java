package com.sec2uniguide.uniguide.services;

import java.util.Optional;

import com.sec2uniguide.uniguide.domains.University;

public interface UniversityService {
	public University save(University university);
	
	public Iterable<University> saveAll(Iterable<University> universities);

	Optional<University> findById(Long id);

	boolean existsById(Long id);
	
	Iterable<University> findAll();

	Iterable<University> findAllById(Iterable<Long> ids);

	University findByUserId(Long user_id);
	
	long count();
	
	void deleteById(Long id);
	
	void delete(University university);
	
	void deleteAll(Iterable<University> universities);

	void deleteAll();
}
