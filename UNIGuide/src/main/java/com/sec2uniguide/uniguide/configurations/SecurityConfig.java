package com.sec2uniguide.uniguide.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.sec2uniguide.uniguide.security.CustomAuthenticationSuccessHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	 
	    @Autowired
	  	private BCryptPasswordEncoder bCryptPasswordEncoder;

	  	@Autowired
	  	private UserDetailsService userDetailsService;
	  	
	  	@Bean("authenticationManager")
	    @Override
	    public AuthenticationManager authenticationManagerBean() throws Exception {
	            return super.authenticationManagerBean();
	    }
	 
	  	
	  	  @Override
	  	  protected void configure(AuthenticationManagerBuilder auth)
	  	      throws Exception {
	  		  auth.userDetailsService(userDetailsService)
	  	          .passwordEncoder(bCryptPasswordEncoder);
	  	  } 
	   
	  	@Bean
	    public AuthenticationSuccessHandler myAuthenticationSuccessHandler(){
	        return new CustomAuthenticationSuccessHandler();
	    }
	  	  
	  	  
	 @Override
	  protected void configure(HttpSecurity http) throws Exception {
		 
		 http.authorizeRequests()
		     .antMatchers("/").permitAll()
		     .antMatchers("/index").permitAll()		     
		     .antMatchers("/educator_login").permitAll()
		     .antMatchers("/**signup").permitAll()
		     .antMatchers("/about").permitAll()
		     .antMatchers("/reset_password").permitAll()
		     .antMatchers("/actuator/**").permitAll()
		     .antMatchers("/universities_main","/scholarships_main",
		    		 "/universities_list","scholarships_list",
		    		 "/educator_profile", "/news", "/profile", "/quiz",
		    		 "/view_profile").hasAuthority("STUDENT")
		     .antMatchers("/educator_post").hasAnyAuthority("AGENT", "UNIVERSITY")
		     .anyRequest().authenticated()
		     .and()
		     	.formLogin()
		     			.loginPage("/")
		     			.loginProcessingUrl("/login")
		     			.failureUrl("/?error=true")
		     			.successHandler(myAuthenticationSuccessHandler())
		     			
		     .and()
		     	.logout()
		     			.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
		     			.logoutSuccessUrl("/")
	     	.and()
		     	.exceptionHandling()
		     	.accessDeniedPage("/access-denied");
	  }
	 
	 @Override
	 public void configure(WebSecurity webSecurity) throws Exception {
		 
		 webSecurity.ignoring()
		 			.antMatchers("/resources/**","/static/**","/css/**","/js/**","/images/**");
		 
	 }
	 
	 
	 
	 
}
