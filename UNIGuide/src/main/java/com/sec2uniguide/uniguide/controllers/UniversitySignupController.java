package com.sec2uniguide.uniguide.controllers;



import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.sec2uniguide.uniguide.domains.University;
import com.sec2uniguide.uniguide.domains.UniversityRegistrationForm;
import com.sec2uniguide.uniguide.security.User;
import com.sec2uniguide.uniguide.services.UniversityService;
import com.sec2uniguide.uniguide.services.UserService;

@Controller
public class UniversitySignupController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UniversityService universityService;
	
	
	@ModelAttribute(name = "form")
	public UniversityRegistrationForm user() {
		return new UniversityRegistrationForm();
		
	}
	
	@GetMapping("/university_signup")
	public String educatorSignup() {
		return "university_signup";
		
	}
	@PostMapping("/university_signup")
    public String createNewUser(@Valid @ModelAttribute("form") UniversityRegistrationForm form, BindingResult bindingResult, Model model) {
        User user = form.getUser();
        University university = form.getUniversity();
        university.setUser(user);
    	User userExists = userService.findUserByUsername(user.getUsername());
        if (userExists != null) {
            bindingResult
                    .rejectValue("user", "error.user",
                            "There is already a user registered with the username provided");
        }
        if (bindingResult.hasErrors()) {
            return "university_signup";
        } else {
        	
            userService.saveUser(user, "UNIVERSITY");
            universityService.save(university);
            model.addAttribute("successMessage", "User has been registered successfully");
            
            return "index";
        }
    }
}

