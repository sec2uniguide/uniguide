package com.sec2uniguide.uniguide.controllers;


import java.util.ArrayList;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import com.sec2uniguide.uniguide.domains.Post;
import com.sec2uniguide.uniguide.domains.Student;
import com.sec2uniguide.uniguide.domains.University;
import com.sec2uniguide.uniguide.security.User;
import com.sec2uniguide.uniguide.services.PostService;
import com.sec2uniguide.uniguide.services.StudentService;
import com.sec2uniguide.uniguide.services.UniversityService;
import com.sec2uniguide.uniguide.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class UniversityMainController {
	
	private UserService userService;
	private StudentService studentService;
	private PostService postService;
	private String username;
	private UniversityService universityService;
	
	@Autowired
	public UniversityMainController(UserService userService, StudentService studentService, PostService postService, UniversityService universityService) {
		this.userService = userService;
		this.studentService = studentService;
		this.postService = postService;
		this.universityService = universityService;
	}
	
	@ModelAttribute(name="student")
	public Student getStudent(@AuthenticationPrincipal UserDetails userDetails, Model model) {
		username = userDetails.getUsername();
		System.out.println(username);
		User user = userService.findUserByUsername(username);
		model.addAttribute("username", username);
		Long id = user.getId();
		Student student = studentService.findByUserId(id);
		return student;
	}
	
	@ModelAttribute(name="postsList")
	public Iterable<Post> getPosts() {
		Iterable<Post> retrieved = postService.findByType("universities");
		ArrayList<Post> postArr = (ArrayList)retrieved;
		ArrayList<Post> posts = new ArrayList<Post>();
		for(int i = postArr.size()-1; i >= 0; i--) {
			posts.add(postArr.get(i));
		}
		return posts;
	}
	
	@ModelAttribute(name="universitiesList")
	public Iterable<University> getUniversities(){
		return universityService.findAll();
	}
	
	@ModelAttribute(name="toPost")
	public Post getPost() {
		return new Post();
	} 
	
	@ModelAttribute(name="postToEdit")
	public Post getPostToEdit() {
		return new Post();
	}
	
	@GetMapping("/universities_main")
	public String universitiesMain() {
		return "universities_main";
		
	}
	
	@PostMapping("/universities_main")
	public String post(@Valid @ModelAttribute("toPost") Post post, Errors errors, @ModelAttribute("student") Student student) {
		if(errors.hasErrors()) {
			System.out.println(errors.getFieldErrors());
			return "/universities_main";
			
		}
		post.setUsername(username);
		post.setStudent(student);
		post.setLikes(0);
		post.setFlag("false");
		post.setType("universities");
		
		Post savedPost = postService.save(post);
		log.info("Post object after persisting: " + savedPost);
		return "redirect:/universities_main";
	}
	
	@DeleteMapping("/universities_main/report")
	public String report(@ModelAttribute("postToEdit") Post post) {
		Post _post = postService.findById(post.getId()).get();	
		postService.delete(_post);
		return "redirect:/universities_main";
	}
	
	@PutMapping("/universities_main/like")
	public String like(@ModelAttribute("postToEdit") Post post) {
		Optional<Post> op = postService.findById(post.getId());
		Post tempPost = op.get();		
		tempPost.setLikes(tempPost.getLikes() + 1);
		postService.save(tempPost);
		return "redirect:/universities_main";
	}
	
}
