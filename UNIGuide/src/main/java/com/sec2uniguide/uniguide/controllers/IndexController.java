package com.sec2uniguide.uniguide.controllers;



import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class IndexController {
	@GetMapping("/index")
	public String educatorSignup() {
		return "index";
		
	}
	
	@GetMapping("/access-denied")
    public String accessDenied(){
        return "access_denied";
    }
}
