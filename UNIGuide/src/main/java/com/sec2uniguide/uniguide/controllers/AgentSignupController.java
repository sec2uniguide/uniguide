package com.sec2uniguide.uniguide.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.sec2uniguide.uniguide.domains.Agent;
import com.sec2uniguide.uniguide.domains.AgentRegistrationForm;
import com.sec2uniguide.uniguide.security.User;
import com.sec2uniguide.uniguide.services.AgentService;
import com.sec2uniguide.uniguide.services.UserService;

@Controller
public class AgentSignupController {

	@Autowired
	private UserService userService;	
	
	@Autowired
	private AgentService agentService;
	
	@ModelAttribute(name = "form")
	public AgentRegistrationForm user() {
		return new AgentRegistrationForm();		
	}
	
	@GetMapping("/agent_signup")
	public String educatorSignup() {
		return "agent_signup";
		
	}
	@PostMapping("/agent_signup")
    public String createNewUser(@Valid @ModelAttribute("form") AgentRegistrationForm form, BindingResult bindingResult, Model model) {
        User user = form.getUser();
        Agent agent = form.getAgent();
        agent.setUser(user);
    	User userExists = userService.findUserByUsername(user.getUsername());
        if (userExists != null) {
            bindingResult
                    .rejectValue("user", "error.user",
                            "There is already a user registered with the username provided");
        }
        if (bindingResult.hasErrors()) {
            return "agent_signup";
        } else {
        	
            userService.saveUser(user, "AGENT");
            agentService.save(agent);
            model.addAttribute("successMessage", "User has been registered successfully");
            
            return "index";
        }
    }
}
