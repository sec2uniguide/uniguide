package com.sec2uniguide.uniguide.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.sec2uniguide.uniguide.domains.Student;
import com.sec2uniguide.uniguide.domains.StudentRegistrationForm;
import com.sec2uniguide.uniguide.security.User;
import com.sec2uniguide.uniguide.services.StudentService;
import com.sec2uniguide.uniguide.services.UserService;


@Controller
public class SignupController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private StudentService studentService;
	
	@ModelAttribute(name = "form")
	public StudentRegistrationForm user() {
		return new StudentRegistrationForm();
		
	}
	
	@GetMapping("/signup")
	public String educatorSignup() {
		return "signup";
		
	}
	
	
	@PostMapping("/signup")
    public String createNewUser(@Valid @ModelAttribute("form") StudentRegistrationForm form, BindingResult bindingResult, Model model) {
        User user = form.getUser();
        Student student = form.getStudent();
        student.setUser(user);
    	User userExists = userService.findUserByUsername(user.getUsername());
        if (userExists != null) {
            bindingResult
                    .rejectValue("form", "error.form",
                            "There is already a user registered with the username provided");
            model.addAttribute("errorMessage", "User name already exists");
        }
        if (bindingResult.hasErrors()) {
            return "signup";
            
        } else {        	
            userService.saveUser(user, "STUDENT");
            studentService.save(student);
            
            return "index";
        }
    }
}
