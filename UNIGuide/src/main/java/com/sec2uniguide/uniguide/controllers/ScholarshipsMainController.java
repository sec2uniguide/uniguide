package com.sec2uniguide.uniguide.controllers;

import java.util.ArrayList;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import com.sec2uniguide.uniguide.domains.Agent;
import com.sec2uniguide.uniguide.domains.Post;
import com.sec2uniguide.uniguide.domains.Student;
import com.sec2uniguide.uniguide.security.User;
import com.sec2uniguide.uniguide.services.AgentService;
import com.sec2uniguide.uniguide.services.PostService;
import com.sec2uniguide.uniguide.services.StudentService;
import com.sec2uniguide.uniguide.services.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class ScholarshipsMainController {
	
	private UserService userService;
	private StudentService studentService;
	private PostService postService;
	private String username;
	private AgentService agentService;
	
	@Autowired
	public ScholarshipsMainController(UserService userService, StudentService studentService, PostService postService, AgentService agentService) {
		this.userService = userService;
		this.studentService = studentService;
		this.postService = postService;
		this.agentService = agentService;
	}
	
	@ModelAttribute(name="student")
	public Student getStudent(@AuthenticationPrincipal UserDetails userDetails, Model model) {
		username = userDetails.getUsername();
		System.out.println(username);
		User user = userService.findUserByUsername(username);
		model.addAttribute("username", username);
		Long id = user.getId();
		Student student = studentService.findByUserId(id);
		return student;
	}
	
	@ModelAttribute(name="postsList")
	public Iterable<Post> getPosts() {
		Iterable<Post> retrieved = postService.findByType("scholarships");
		ArrayList<Post> postArr = (ArrayList)retrieved;
		ArrayList<Post> posts = new ArrayList<Post>();
		for(int i = postArr.size()-1; i >= 0; i--) {
			posts.add(postArr.get(i));
		}
		return posts;
	}
	
	@ModelAttribute(name="agentList")
	public Iterable<Agent> getAgents(){
		return agentService.findAll();
	}
	
	@ModelAttribute(name="toPost")
	public Post getPost() {
		return new Post();
	}
	
	@GetMapping("/scholarships_main")
	public String scholarshipsMain() {
		return "scholarships_main";
		
	}
	
	@PostMapping("/scholarships_main")
	public String post(@Valid @ModelAttribute("toPost") Post post, Errors errors, @ModelAttribute("student") Student student) {
		if(errors.hasErrors()) {
			System.out.println(errors.getFieldErrors());
			return "/scholarships_main";
			
		}
		post.setUsername(username);
		post.setStudent(student);
		post.setLikes(0);
		post.setFlag("false");
		post.setType("scholarships");
		
		Post savedPost = postService.save(post);
		log.info("Post object after persisting: " + savedPost);
		return "redirect:/scholarships_main";
	}
	@ModelAttribute(name = "postToEdit")
	public Post getPostToEdit() 
	{
		return new Post();
	}
	
	@PutMapping("/scholarships_main/like")
	public String like (@ModelAttribute("postToEdit") Post post) {
		Optional<Post> optionalPost=postService.findById(post.getId());
		Post tempPost = optionalPost.get();
		tempPost.setLikes(tempPost.getLikes() + 1);
		postService.save(tempPost);
		return "redirect:/scholarships_main";
	}
	
	@DeleteMapping("/scholarships_main/report")
	public String report(@ModelAttribute("postToEdit")Post post)
	{
		Optional<Post> op = postService.findById(post.getId());
		Post tempPost = op.get();
		postService.delete(tempPost);
		return "redirect:/scholarships_main";
		
	}
}
