package com.sec2uniguide.uniguide.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.sec2uniguide.uniguide.domains.University;
import com.sec2uniguide.uniguide.services.UniversityService;

	
	@Controller
	public class UniversitiesListController {

		private UniversityService universityService;
		
		public UniversitiesListController(UniversityService universityService) {
			this.universityService = universityService;
		}
		
		@GetMapping("/universities_list")
		public String universitiesList() {
			return "universities_list";
		}
		
		@ModelAttribute(name="universitiesList")
		public Iterable<University> getUniversities(){
			return universityService.findAll();
		}
	}



