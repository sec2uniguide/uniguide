package com.sec2uniguide.uniguide.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.sec2uniguide.uniguide.domains.News;
import com.sec2uniguide.uniguide.domains.Post;
import com.sec2uniguide.uniguide.services.NewsService;


@Controller
public class NewsController {
	
	private NewsService newsService;
	
	@Autowired
	public NewsController(NewsService newsService) {		
		this.newsService = newsService;
		
	}
	
	@ModelAttribute(name="news")
	public Iterable<News> getPosts() {
		Iterable<News> retrieved = newsService.findAll();
		ArrayList<News> newsArr = (ArrayList)retrieved;
		ArrayList<News> news = new ArrayList<News>();
		for(int i = newsArr.size()-1; i >= 0; i--) {
			news.add(newsArr.get(i));
		}
		return news;
	} 
	
	@GetMapping("/news")
	public String news() {
		return "news";
		
	}
}
