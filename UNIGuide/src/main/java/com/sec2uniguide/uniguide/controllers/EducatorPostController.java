package com.sec2uniguide.uniguide.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.sec2uniguide.uniguide.domains.Agent;
import com.sec2uniguide.uniguide.domains.News;
import com.sec2uniguide.uniguide.domains.Post;
import com.sec2uniguide.uniguide.domains.Student;
import com.sec2uniguide.uniguide.security.Role;
import com.sec2uniguide.uniguide.security.User;
import com.sec2uniguide.uniguide.services.AgentService;
import com.sec2uniguide.uniguide.services.NewsService;
import com.sec2uniguide.uniguide.services.UniversityService;
import com.sec2uniguide.uniguide.services.UserService;

@Controller
public class EducatorPostController {	
	
	private UserService userService;
	private NewsService newsService;
	private AgentService agentService;
	private UniversityService universityService;
	private String username;
	
	@Autowired
	public EducatorPostController(UserService userService, NewsService newsService, AgentService agentService, UniversityService universityService) {
		this.userService = userService;
		this.newsService = newsService;
		this.agentService = agentService;
		this.universityService = universityService;
	}
	
	@ModelAttribute(name="news")
	public News post() {
		return new News();
	}	
	
	@GetMapping("/educator_post")
	public String educatorSignup(@AuthenticationPrincipal UserDetails userDetails, Model model) {
		username = userDetails.getUsername();
		System.out.println("Username: " + username );
		User user = userService.findUserByUsername(username);
		
		Role userRole = (Role)user.getRoles().toArray()[0];
		System.out.println("User role: " + userRole.getRole() );
		if(userRole.getRole().equals("AGENT")) {
			System.out.println("In if User role: " + userRole.getRole() );
			model.addAttribute("educator", agentService.findByUserId(user.getId()));
		}else if(userRole.getRole().equals("UNIVERSITY")) {
			model.addAttribute("educator", universityService.findByUserId(user.getId()));
		}
		
		System.out.println("------------\nEducator: " +  agentService.findByUserId(user.getId()));
		
		return "educator_post";
		
	}
	
	@PostMapping("/educator_post")
	public String postNews(@Valid @ModelAttribute("news") News news, Errors errors) {
		System.out.println("In Post: ");
		if(errors.hasErrors()) {
			System.out.println("Error: ");
			return "/educator_post";
		}
		System.out.println("Noerror: ");
		news.setAuthor(username);
		newsService.save(news);
		
		return "redirect:/educator_post";
	}
	
	
	
}
