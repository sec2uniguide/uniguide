package com.sec2uniguide.uniguide.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class EducatorProfileController {
	@GetMapping("/educator_profile")
	public String educatorProfile() {
		return "educator_profile";
		
	}
}
