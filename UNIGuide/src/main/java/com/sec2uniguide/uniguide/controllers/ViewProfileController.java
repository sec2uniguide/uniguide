package com.sec2uniguide.uniguide.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
@Controller
public class ViewProfileController {
	@GetMapping("/view_profile")
	public String viewProfile() {
		return "view_profile";
		
	}
}
