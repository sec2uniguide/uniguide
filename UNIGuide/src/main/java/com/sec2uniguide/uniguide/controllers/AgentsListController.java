package com.sec2uniguide.uniguide.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.sec2uniguide.uniguide.domains.Agent;
import com.sec2uniguide.uniguide.services.AgentService;


@Controller
public class AgentsListController {
	
	private AgentService agentService;
	
	public AgentsListController(AgentService agentService) {
		this.agentService = agentService;
	}
	
	@GetMapping("/scholarships_list")
	public String scholarshipsList() {
		return "scholarships_list";
	}
	
	@ModelAttribute(name="agentList")
	public Iterable<Agent> getAgents(){
		return agentService.findAll();
	}
}
