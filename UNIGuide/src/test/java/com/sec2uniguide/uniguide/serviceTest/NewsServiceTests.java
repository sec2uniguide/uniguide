package com.sec2uniguide.uniguide.serviceTest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.sec2uniguide.uniguide.domains.News;
import com.sec2uniguide.uniguide.repositories.NewsRepository;
import com.sec2uniguide.uniguide.services.NewsService;
import com.sec2uniguide.uniguide.services.NewsServiceImpl;

@RunWith(SpringRunner.class)
public class NewsServiceTests {

	@TestConfiguration
	static class PostServiceImplTestContextConfiguration {
	  
	        @Bean
	        public NewsService newsService() {
	            return new NewsServiceImpl();
	        }
	    }
	
	@Autowired
    private NewsService newsService;
    
	@MockBean
	private static NewsRepository newsRepository;	
    
	@Before
	public void setUp() {
	    News news = new News();
	    news.setTitle("news");
	    
	    Mockito.when(newsRepository.findById(1l))
	      .thenReturn(Optional.of(news));
	}
	
    @Test
    public void whenValidName_thenEmployeeShouldBeFound() {
        Optional<News> found = newsService.findById(1l);
      
         assertThat(found.get().getTitle())
          .isEqualTo("news");
     }
}
