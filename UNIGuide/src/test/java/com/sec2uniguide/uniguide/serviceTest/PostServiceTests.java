package com.sec2uniguide.uniguide.serviceTest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.sec2uniguide.uniguide.domains.Agent;
import com.sec2uniguide.uniguide.domains.Post;
import com.sec2uniguide.uniguide.repositories.AgentRepository;
import com.sec2uniguide.uniguide.repositories.PostRepository;
import com.sec2uniguide.uniguide.services.AgentService;
import com.sec2uniguide.uniguide.services.AgentServiceImpl;
import com.sec2uniguide.uniguide.services.PostService;
import com.sec2uniguide.uniguide.services.PostServiceImpl;

@RunWith(SpringRunner.class)
public class PostServiceTests {

	@TestConfiguration
	static class PostServiceImplTestContextConfiguration {
	  
	        @Bean
	        public PostService postService() {
	            return new PostServiceImpl();
	        }
	    }
	
	@Autowired
    private PostService postService;
    
	@MockBean
	private static PostRepository postRepository;	
    
	@Before
	public void setUp() {
	    Post post = new Post();
	    post.setType("post");
	    
	    Mockito.when(postRepository.findById(1l))
	      .thenReturn(Optional.of(post));
	}
	
    @Test
    public void whenValidName_thenEmployeeShouldBeFound() {
        Optional<Post> found = postService.findById(1l);
      
         assertThat(found.get().getType())
          .isEqualTo("post");
     }
}
