package com.sec2uniguide.uniguide.serviceTest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.sec2uniguide.uniguide.domains.University;
import com.sec2uniguide.uniguide.repositories.UniversityRepository;
import com.sec2uniguide.uniguide.services.UniversityService;
import com.sec2uniguide.uniguide.services.UniversityServiceImpl;

@RunWith(SpringRunner.class)
public class UniversityServiceTests {

	@TestConfiguration
	static class UniversityServiceImplTestContextConfiguration {
	  
	        @Bean
	        public UniversityService universityService() {
	            return new UniversityServiceImpl();
	        }
	    }
	
	@Autowired
    private UniversityService universityService;
    
	@MockBean
	private static UniversityRepository universityRepository;	
    
	@Before
	public void setUp() {
	    University university = new University();
	    university.setName("university");
	    
	    Mockito.when(universityRepository.findById(1l))
	      .thenReturn(Optional.of(university));
	}
	
    @Test
    public void whenValidName_thenEmployeeShouldBeFound() {
        Optional<University> found = universityService.findById(1l);
      
         assertThat(found.get().getName())
          .isEqualTo("university");
     }
}
