package com.sec2uniguide.uniguide.serviceTest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.sec2uniguide.uniguide.repositories.RoleRepository;
import com.sec2uniguide.uniguide.security.Role;
import com.sec2uniguide.uniguide.services.RoleService;
import com.sec2uniguide.uniguide.services.RoleServiceImpl;

@RunWith(SpringRunner.class)
public class RoleServiceTests {

	@TestConfiguration
	static class RoleServiceImplTestContextConfiguration {
	  
	        @Bean
	        public RoleService roleService() {
	            return new RoleServiceImpl();
	        }
	    }
	
	@Autowired
    private RoleService roleService;
    
	@MockBean
	private static RoleRepository roleRepository;	
    
	@Before
	public void setUp() {
	    Role role = new Role();
	    role.setRole("STUDENT");;
	    
	    Mockito.when(roleRepository.findById(1l))
	      .thenReturn(Optional.of(role));
	}
	
    @Test
    public void whenValidName_thenEmployeeShouldBeFound() {
        Optional<Role> found = roleService.findById(1l);
      
         assertThat(found.get().getRole())
          .isEqualTo("STUDENT");
     }
}
