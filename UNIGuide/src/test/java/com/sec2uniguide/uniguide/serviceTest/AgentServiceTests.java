package com.sec2uniguide.uniguide.serviceTest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.sec2uniguide.uniguide.domains.Agent;
import com.sec2uniguide.uniguide.repositories.AgentRepository;
import com.sec2uniguide.uniguide.services.AgentService;
import com.sec2uniguide.uniguide.services.AgentServiceImpl;

@RunWith(SpringRunner.class)
public class AgentServiceTests {
	
	@TestConfiguration
	static class AgentServiceImplTestContextConfiguration {
	  
	        @Bean
	        public AgentService agentService() {
	            return new AgentServiceImpl();
	        }
	    }
	
	@Autowired
    private AgentService agentService;
    
	@MockBean
	private static AgentRepository agentRepository;	
    
	@Before
	public void setUp() {
	    Agent agent = new Agent();
	    agent.setName("agent");
	    
	    Mockito.when(agentRepository.findById(1l))
	      .thenReturn(Optional.of(agent));
	}
	
    @Test
    public void whenValidName_thenEmployeeShouldBeFound() {
        Optional<Agent> found = agentService.findById(1l);
      
         assertThat(found.get().getName())
          .isEqualTo("agent");
     }
}
