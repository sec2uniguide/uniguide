package com.sec2uniguide.uniguide.serviceTest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.sec2uniguide.uniguide.domains.Student;
import com.sec2uniguide.uniguide.repositories.StudentRepository;
import com.sec2uniguide.uniguide.services.StudentService;
import com.sec2uniguide.uniguide.services.StudentServiceImpl;

@RunWith(SpringRunner.class)
public class StudentServiceTests {

	@TestConfiguration
	static class StudentServiceImplTestContextConfiguration {
	  
	        @Bean
	        public StudentService studentService() {
	            return new StudentServiceImpl();
	        }
	    }
	
	@Autowired
    private StudentService studentService;
    
	@MockBean
	private static StudentRepository studentRepository;	
    
	@Before
	public void setUp() {
	    Student student = new Student();
	    student.setName("student");
	    
	    Mockito.when(studentRepository.findById(1l))
	      .thenReturn(Optional.of(student));
	}
	
    @Test
    public void whenValidName_thenEmployeeShouldBeFound() {
        Optional<Student> found = studentService.findById(1l);
      
         assertThat(found.get().getName())
          .isEqualTo("student");
     }
}
