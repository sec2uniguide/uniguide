package com.sec2uniguide.uniguide.serviceTest;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.sec2uniguide.uniguide.repositories.RoleRepository;
import com.sec2uniguide.uniguide.repositories.UserRepository;
import com.sec2uniguide.uniguide.security.User;
import com.sec2uniguide.uniguide.services.UserService;
import com.sec2uniguide.uniguide.services.UserServiceImpl;

@RunWith(SpringRunner.class)
public class UserServiceTests {

	@TestConfiguration
	static class UserServiceImplTestContextConfiguration {
	  
	        @Bean
	        public UserService userService() {
	            return new UserServiceImpl();
	        }
	    }
	
	@Autowired
    private UserService userService;
    
	@MockBean
	private static UserRepository userRepository;	
	
	
	@MockBean
	private static RoleRepository roleRepository;
	
	 @MockBean
	 private BCryptPasswordEncoder bCryptPasswordEncoder;
    
	@Before
	public void setUp() {
	    User user = new User();
	    user.setUsername("user");
	    
	    Mockito.when(userRepository.findByUsername("user"))
	      .thenReturn(user);
	}
	
    @Test
    public void whenValidName_thenEmployeeShouldBeFound() {
        User found = userService.findUserByUsername("user");
      
         assertThat(found.getUsername())
          .isEqualTo("user");
     }
}
