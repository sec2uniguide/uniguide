package com.sec2uniguide.uniguide.controllerTests;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.sec2uniguide.uniguide.controllers.IndexController;
import com.sec2uniguide.uniguide.controllers.ScholarshipsMainController;
import com.sec2uniguide.uniguide.services.AgentService;
import com.sec2uniguide.uniguide.services.PostService;
import com.sec2uniguide.uniguide.services.StudentService;
import com.sec2uniguide.uniguide.services.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(ScholarshipsMainController.class)
public class ScholarshipsMainControllerTests {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	UserService userService;
	
	@MockBean
	StudentService studentService;
	
	@MockBean
	AgentService agentService;
	
	@MockBean
	PostService postService;
	
	
	@Test
	@WithMockUser
	public void testScholarshipsMainController_get() throws Exception {
		/*mockMvc.perform(get("/scholarships_main"))  
		
			.andExpect(status().isOk()) 
			
			.andExpect(view().name("scholarships_main"))  
			  
			.andExpect(content().string(containsString("Ethiopian Universities")));  
	*/}
	
}
