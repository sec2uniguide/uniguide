package com.sec2uniguide.uniguide.controllerTests;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.sec2uniguide.uniguide.controllers.IndexController;
import com.sec2uniguide.uniguide.controllers.UniversitySignupController;
import com.sec2uniguide.uniguide.services.StudentService;
import com.sec2uniguide.uniguide.services.UniversityService;
import com.sec2uniguide.uniguide.services.UserService;

@RunWith(SpringRunner.class)
@WebMvcTest(UniversitySignupController.class)
public class UniversitySignupControllerTests {


	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	UserService userService;
	
	@MockBean
	UniversityService universityService;	
	
	@Test
	public void testUniversitySignupController_get() throws Exception {
		mockMvc.perform(get("/university_signup"))  
		
			.andExpect(status().isOk()) 
			
			.andExpect(view().name("university_signup"))  
			  
			.andExpect(content().string(containsString("SIGNUP")));  
	}

}
